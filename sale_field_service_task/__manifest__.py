# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    For Module Support : mayurmaheshwari07@gmail.com  or Skype : mayur_maheshwari1
#
##############################################################################

{
    'name': 'Sale Field Service Task',
    'version': '13.0.1.0',
    'sequence': 1,
    'category': 'Generic Modules/Sales Management',
    'description':
        """
        This Module add below functionality into odoo

        1.Sale Field Service Task\n

    """,
    'summary': 'Sale Field Service Task',
    'author': 'Mayur Maheshwari', 
    'depends': ['sale_management','industry_fsm','project'],
    'data': [
        'views/res_config_views.xml',
        # 'views/sale_view.xml',
        ],
    'demo': [],
    'test': [],
    'css': [],
    'qweb': [],
    'js': [],
    'images': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
