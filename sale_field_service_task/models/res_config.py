# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    For Module Support : mayurmaheshwari07@gmail.com  or Skype : mayur_maheshwari1
#
##############################################################################

from odoo import models, fields, api


class ResCompany(models.Model):
    _inherit = 'res.company'

    project_id = fields.Many2one('project.project',string='Project' ,readonly=False)
    

class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    project_id = fields.Many2one(related='company_id.project_id',readonly=False, string='Project')
    


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
