# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    For Module Support : mayurmaheshwari07@gmail.com  or Skype : mayur_maheshwari1
#
##############################################################################
from . import res_config
from . import sale


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
