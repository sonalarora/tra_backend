# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
##############################################################################

{
    'name': 'MKS Delivery',
    'version': '13.0.1.0',
    'sequence': 1,
    'category': 'Generic Modules/Sales Management',
    'description':
        """
        This Module add below functionality into odoo

        1.Sale Field Service Task\n

    """,
    'summary': 'MKS Delivery',
    'author': 'Masterkey', 
    'website': 'irfan@masterkeyglobal.com',
    'depends': ['sale_management','industry_fsm','project','contacts'],
    'data': [
        'security/ir.model.access.csv',
        'views/res_config_views.xml',
        'views/sale_view.xml',
        'views/zone_form_view.xml',
        'views/city_form_view.xml',
        'views/res_partner_view.xml',
        'views/project_task_view.xml',
        ],
    'demo': [],
    'test': [],
    'css': [],
    'qweb': [],
    'js': [],
    'images': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
