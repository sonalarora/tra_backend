# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
##############################################################################

from odoo import models, fields, api


class zone_zone(models.Model):
	_name = 'zone.zone'
	
	name = fields.Char(string="Name",required=True)
	code = fields.Char(string="Code") 
	zone_ids = fields.One2many('city.city','zone_id',string='Zone', \
                                copy=False)
                        
	

	




# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
