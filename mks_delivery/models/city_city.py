# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#
##############################################################################

from odoo import models, fields, api


class city_city(models.Model):
	_name = 'city.city'
	
	name = fields.Char(string="Name")
	code = fields.Char(string="Code") 
	zone_id = fields.Many2one('zone.zone',string="Zone")
	

	




# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
