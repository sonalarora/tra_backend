# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#
##############################################################################

from . import res_config
from . import sale
from . import zone_zone
from . import res_partner
from . import city_city
from . import project_task


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
