# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#
##############################################################################

from odoo import models, fields, api,_
from odoo.exceptions import ValidationError

class project_task(models.Model):
    _inherit='project.task'
    
    
    state = fields.Selection([
        ('unassigned', 'Unassigned'),
        ('assigned','Assigned'),('accepted','Accepted'),
        ('started','Started'),('delivered','Delivered'),
        ('collected','Collected'),
        ('cancel','Cancel'),('issue','Issue'),
        ('paid','Paid'),('non_paid','Non Paid'),
        ], string='Status', readonly=True, copy=False, default='unassigned')
    
    zone_id = fields.Many2one('zone.zone',string="Zone")
    
    def action_assign(self):
        self.state = 'assigned'
        return True
    
    def action_accept(self):
        self.state = 'accepted'
        return True
        
    def action_start(self):
        self.state = 'started'
        return True
    
    def action_delivery(self):
        self.state = 'delivered'
        return True

    def action_collect(self):
        self.state = 'collected'
        return True
    
    def action_cancel(self):
        self.state = 'cancel'
        return True
    
    def action_issue(self):
        self.state = 'issue'
        return True
        
    def action_paid(self):
        self.state = 'paid'
        return True
    
    def action_non_paid(self):
        self.state = 'non_paid'
        return True
        
    
    
    
    
        
    
    

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
