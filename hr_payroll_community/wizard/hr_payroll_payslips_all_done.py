# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class MultiHRPayslipDoneWiz(models.TransientModel):
    _name = 'multi.hr.payslip.done.wiz'

    def done_multi_hr_payslip(self):
        hr_payslip_ids = self.env['hr.payslip'].browse(self._context.get('active_ids'))
        for payslip in hr_payslip_ids:
            if payslip.state == 'draft':
                payslip.action_payslip_done()
