# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import purchase_requisition
from . import purchase_order
from . import stock_picking
from . import sale_order
from . import account_invoice
