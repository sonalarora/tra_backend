# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, SUPERUSER_ID, _

class stock_picking(models.Model):
    _inherit = "stock.picking"
    
    state = fields.Selection(selection_add=[('qa_approval', 'QA Validation')],
    )
    
    def button_validate_qa_approval(self):
        self.state = 'qa_approval'
        return True
    
    
