# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, SUPERUSER_ID, _

class account_move(models.Model):
    _inherit = "account.move"
    
    state = fields.Selection(selection_add=[('approval','Financial Approval')])
    
    def make_invoice_url(self):
        for invoice in self:
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url', default='http://localhost:8069')
            if base_url:
                base_url += '/web/login?db=%s&login=%s&key=%s#id=%s&model=%s' % (
                self._cr.dbname, '', '', invoice.id, 'account.move')
                return base_url
    
    def get_email(self):
        group_id = self.env['ir.model.data'].get_object_reference('account', 'group_account_manager')[1]
        email = ''
        if group_id:
            group_id = self.env['res.groups'].browse(group_id)
            for user in group_id.users:
                if user.partner_id.email:
                    if email:
                        email = email+','+user.partner_id.email
                    else:
                        email = user.partner_id.email
        return email
    
    def send_invoice_approval_mail(self):
        email = self.get_email()
        if email:
            mtp =self.env['mail.template']
            ir_model_data = self.env['ir.model.data']
            template_id = ir_model_data.get_object_reference('MKS_Tradex_Backend_2', 'invoice_approval_mail_template')
            mail_tem=mtp.browse(template_id[1])
            mail_tem.write({'email_to': email})
            mail_tem.send_mail(self.id,True)
            
    
    def action_post(self):
        if self.env.user.has_group('account.group_account_manager'):
            return super(account_move,self).action_post()
        else:
            if self.type in ['out_invoice','out_refund','in_invoice','in_refund'] and self.state == 'draft':
                self.send_invoice_approval_mail()
                self.state = 'approval'
            else:
                return super(account_move,self).action_post()
    
