# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, SUPERUSER_ID, _

class purchase_order(models.Model):
    _inherit = "purchase.order"
    
    state = fields.Selection([
        ('draft', 'RFQ'),
        ('sent', 'RFQ Sent'),
        ('approved', 'RFQ Submitted To MGT'),
        ('po_approval', 'Approved By MGT'),
        ('to approve', 'To Approve'),
        ('purchase', 'Purchase Order'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled')
    ], string='Status', readonly=True, index=True, copy=False, default='draft', tracking=True)
    
    def make_url(self):
        record_id = self.id
        menu_id = self.env.ref('purchase.menu_purchase_rfq').id
        action_id = self.env.ref('purchase.purchase_rfq').id
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        if base_url:
            base_url += \
                '/web?#id=%s&view_type=form&model=%s&menu_id=%s&action=%s' % (
                    self.id, self._name, menu_id, action_id)
        return base_url
    
    def action_manager_approval(self):
        group_id = self.env['ir.model.data'].get_object_reference('MKS_Tradex_Backend_2', 'group_purchase_coordinator')[1]
        if group_id:
            browse_group = self.env['res.groups'].browse(group_id)
            for user in browse_group.users:
                manager_mail = user.partner_id.email
                mtp =self.env['mail.template']
                ir_model_data = self.env['ir.model.data']
                template_id = ir_model_data.get_object_reference('MKS_Tradex_Backend_2', 'purchase_req_coordinator_template')
                mail_tem=mtp.browse(template_id[1])
                mail_tem.send_mail(self.id,True)
                mail_tem.write({'email_to': manager_mail})
        self.state = 'approved'
    
    def button_confirm_mks(self):
        group_id = self.env['ir.model.data'].get_object_reference('purchase', 'group_purchase_manager')[1]
        if group_id:
            browse_group = self.env['res.groups'].browse(group_id)
            for user in browse_group.users:
                manager_mail = user.partner_id.email
                mtp =self.env['mail.template']
                ir_model_data = self.env['ir.model.data']
                template_id = ir_model_data.get_object_reference('MKS_Tradex_Backend_2', 'purchase_req_manager_template')
                mail_tem=mtp.browse(template_id[1])
                mail_tem.send_mail(self.id,True)
                mail_tem.write({'email_to': manager_mail})
        self.state = 'po_approval'
        return True
    
        
    def button_confirm(self):
        for order in self:
            if order.state not in ['draft', 'sent','approved','po_approval']:
                continue
            order._add_supplier_to_product()
            # Deal with double validation process
            if order.company_id.po_double_validation == 'one_step'\
                    or (order.company_id.po_double_validation == 'two_step'\
                        and order.amount_total < self.env.company.currency_id._convert(
                            order.company_id.po_double_validation_amount, order.currency_id, order.company_id, order.date_order or fields.Date.today()))\
                    or order.user_has_groups('purchase.group_purchase_manager'):
                order.button_approve()
            else:
                order.write({'state': 'to approve'})
        return True
