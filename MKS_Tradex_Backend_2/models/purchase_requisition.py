# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, SUPERUSER_ID, _


PURCHASE_REQUISITION_STATES = [
    ('draft', 'New'),
    ('approval', 'Submitted To PC'),
    ('validate_by_pc','Validated By PC'),
    ('manager_approval','Submitted To MGT'),
    ('approved','Approved By MGT'),
    ('ongoing', 'Ongoing'),
    ('in_progress', 'Approved By MGT'),
    ('open', 'Bid Selection'),
    ('done', 'Closed'),
    ('cancel', 'Cancelled'),
]

class purchase_requisition(models.Model):
    _inherit = "purchase.requisition"

    # def _check_store_keeper(self):
    #     print('store_keeper_group++++++++++++++++++++++')
    #     store_keeper_group = self.user_has_groups('purchase.group_purchase_user')
    #     if store_keeper_group:
    #         self.store_keeper_visibility = True
    #     else:
    #         self.store_keeper_visibility = False

    
    state = fields.Selection(PURCHASE_REQUISITION_STATES,
                              'Status', tracking=True, required=True,
                              copy=False, default='draft')
    
    state_blanket_order = fields.Selection(PURCHASE_REQUISITION_STATES, compute='_set_state')
    # store_keeper_visibility = fields.Boolean("Visibility",default='_check_store_keeper')


        
    
    @api.depends('state')
    def _set_state(self):
        self.state_blanket_order = self.state

    def basick_information(self):
        order_table=''
        order_table +='''
                <table border=1 width=100% style='margin-top: 10px;'>
                <tr>
                    <td width="20%"><center><b>Purchase Representative</b></center></td>
                    <td width="20%"><center><b>Agreement Type</b></center></td>
                 	<td width="20%"><center><b>Vendor</b></center></td>
                 	<td width="20%"><center><b>Agreement Deadline</b></center></td>
                 	<td width="20%"><center><b>Delivery Date</b></center></td>
                </tr>
                '''
        Purchase_rep = self.user_id and self.user_id.name or ' '
        agreement_type = self.type_id and self.type_id.name or ' '
        vendor = self.vendor_id and self.vendor_id.name or ' '
        agreement_deadline = self.date_end  or ' '
        delivery_date = self.schedule_date  or ' '
        order_table += "<tr>" + '<td align="center">' + Purchase_rep  + '</td>' + '<td align="center">' + str(agreement_type) + '</td>' + '<td align="center">' + vendor + '</td>' + '<td align="center">' + str(agreement_deadline) + '</td>' + '<td align="center">' + str(delivery_date) + '</td>' + "</tr>"
		
        order_table += '''
						</table>
                        '''
                        
        return order_table    
        

    def line_information(self):
        order_table=''
        order_table +='''
                <table border=1 width=100% style='margin-top: 10px;'>
                <tr>
                    <td width="40%"><left><b>Product</b></center></td>
                    <td width="15%"><center><b>Quantity</b></center></td>
                 	<td width="15%"><center><b>Order Quantity </b></center></td>
                 	<td width="15%"><center><b>Scheduled Date</b></center></td>
                 	<td width="15%"><center><b>Unit Price</b></center></td>
                </tr>
                '''
        for line in self.line_ids:
            product_name = line.product_id and line.product_id.name or ' '
            schedule_date = line.schedule_date or ' '
            order_table += "<tr>" + '<td align="left">' + product_name  + '</td>' + '<td align="center">' + str(line.product_qty) + '</td>' + '<td align="center">' + str(line.qty_ordered) + '</td>' + '<td align="center">' + str(schedule_date) + '</td>' + '<td align="center">' + str(line.price_unit) + '</td>' + "</tr>"
        order_table += '''
					    </table>
                        '''
                        
        return order_table    



    def make_url(self):
        record_id = self.id
        menu_id = self.env.ref('purchase_requisition.menu_purchase_requisition_pro_mgt').id
        action_id = self.env.ref('purchase_requisition.action_purchase_requisition').id
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        if base_url:
            base_url += \
                '/web?#id=%s&view_type=form&model=%s&menu_id=%s&action=%s' % (
                    self.id, self._name, menu_id, action_id)
        return base_url
    
    def action_approval(self):
        group_id = self.env['ir.model.data'].get_object_reference('MKS_Tradex_Backend_2', 'group_purchase_coordinator')[1]
        if group_id:
            browse_group = self.env['res.groups'].browse(group_id)
            for user in browse_group.users:
                coordinator_mail = user.partner_id.email
                mtp =self.env['mail.template']
                ir_model_data = self.env['ir.model.data']
                template_id = ir_model_data.get_object_reference('MKS_Tradex_Backend_2', 'purchase_coordinator_template')
                mail_tem=mtp.browse(template_id[1])
                mail_tem.send_mail(self.id,True)
                mail_tem.write({'email_to': coordinator_mail})
        self.state = 'approval'
        
    def action_coordinator_approval(self):
        group_id = self.env['ir.model.data'].get_object_reference('MKS_Tradex_Backend_2', 'group_purchase_coordinator')[1]
        if group_id:
            browse_group = self.env['res.groups'].browse(group_id)
            for user in browse_group.users:
                manager_mail = user.partner_id.email
                mtp =self.env['mail.template']
                ir_model_data = self.env['ir.model.data']
                template_id = ir_model_data.get_object_reference('MKS_Tradex_Backend_2', 'purchase_coordinator_template')
                mail_tem=mtp.browse(template_id[1])
                mail_tem.send_mail(self.id,True)
                mail_tem.write({'email_to': manager_mail})
        self.state = 'validate_by_pc'

    def action_submitted_to_mgt(self):
        group_id = self.env['ir.model.data'].get_object_reference('purchase', 'group_purchase_manager')[1]
        if group_id:
            browse_group = self.env['res.groups'].browse(group_id)
            for user in browse_group.users:
                manager_mail = user.partner_id.email
                mtp =self.env['mail.template']
                ir_model_data = self.env['ir.model.data']
                template_id = ir_model_data.get_object_reference('MKS_Tradex_Backend_2', 'purchase_manager_template')
                mail_tem=mtp.browse(template_id[1])
                mail_tem.send_mail(self.id,True)
                mail_tem.write({'email_to': manager_mail})
        self.state = 'manager_approval'

        
    def action_manager_approval(self):
        self.state = 'manager_approval'


class PurchaseRequisitionLine(models.Model):
    _inherit = "purchase.requisition.line"

    state = fields.Selection(related='requisition_id.state', store=True, readonly=False,default='draft')

