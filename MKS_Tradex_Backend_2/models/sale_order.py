# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, SUPERUSER_ID, _

class sale_order(models.Model):
    _inherit = "sale.order"

    #    logistics_id = fields.Many2one('res.users',string="Logistics Users")
    #    credit_controller_id = fields.Many2one('res.users',string="Credit Controller")
    
    state = fields.Selection([
        ('draft', 'new'),
        ('approval', 'Submitted To MGT'),
        ('approved', 'Approved By MGT'),
        ('logistics_approval', 'Validated By LC'),
        ('logistics_approved', 'validated By LC'),
        ('credit_approval', 'Submitted To CC'),
        ('sent', 'Quotation Sent'),
        ('sale', 'Validated By CC'),
        ('done', 'Locked'),
        ('reject','Rejected By MGT'),
        ('cancel', 'Cancelled'),
        ], string='Status', readonly=True, copy=False, index=True, tracking=3, default='draft')

    rental_status = fields.Selection([
        ('draft', 'Quotation'),
        ('approval', 'Submitted To MGT'),
        ('approved', 'Approved By MGT'),
        ('logistics_approval', 'Validated By LC'),
        ('credit_approval', 'Submitted To CC'),
        ('sent', 'Quotation Sent'),
        ('pickup', 'Reserved'),
        ('return', 'Picked-up'),
        ('returned', 'Returned'),
        ('cancel', 'Cancelled'),
    ], string="Rental Status", compute='_compute_rental_status', store=True)   

    def make_url(self):
        record_id = self.id
        menu_id = self.env.ref('sale.menu_sale_quotations').id
        action_id = self.env.ref('sale.action_quotations_with_onboarding').id
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        if base_url:
            base_url += \
                '/web?#id=%s&view_type=form&model=%s&menu_id=%s&action=%s' % (
                    self.id, self._name, menu_id, action_id)
        return base_url


    def action_confirm_approval_request(self):
        group_id = self.env['ir.model.data'].get_object_reference('sales_team', 'group_sale_manager')[1]
        if group_id:
            browse_group = self.env['res.groups'].browse(group_id)
            for user in browse_group.users:
                print ("=====",user.name)
                manager_mail = user.partner_id.email
                mtp =self.env['mail.template']
                ir_model_data = self.env['ir.model.data']
                template_id = ir_model_data.get_object_reference('MKS_Tradex_Backend_2', 'sale_manager_template')
                mail_tem=mtp.browse(template_id[1])
                mail_tem.write({'email_to': manager_mail})
                mail_tem.send_mail(self.id,True)
        self.state = 'approval'
        return True

    def action_manager_approval(self):
        user_email = self.user_id.partner_id.email
        manager = self.env.user.partner_id.email
        mtp =self.env['mail.template']
        ir_model_data = self.env['ir.model.data']
        template_id = ir_model_data.get_object_reference('MKS_Tradex_Backend_2', 'sale_user_template')
        mail_tem=mtp.browse(template_id[1])
        mail_tem.send_mail(self.id,True)
        mail_tem.write({'email_from': manager,'email_to': user_email})
        self.state = 'approved'
        return True

    def action_managment_reject(self):
        self.state = 'reject'
        return True

    def reset(self):
        self.state = 'approved'
        return True

        
    def action_logistics_approval(self):
        group_id = self.env['ir.model.data'].get_object_reference('MKS_Tradex_Backend_2', 'group_sale_logistics')[1]
        if group_id:
            browse_group = self.env['res.groups'].browse(group_id)
            for user in browse_group.users:
                manager_mail = user.partner_id.email
                mtp =self.env['mail.template']
                ir_model_data = self.env['ir.model.data']
                template_id = ir_model_data.get_object_reference('MKS_Tradex_Backend_2', 'sale_logistic_approval_template')
                mail_tem=mtp.browse(template_id[1])
                mail_tem.send_mail(self.id,True)
                mail_tem.write({'email_to': manager_mail})
        self.state = 'logistics_approval'
        return True
    
    def action_logistics_approve(self):
        group_id = self.env['ir.model.data'].get_object_reference('MKS_Tradex_Backend_2', 'group_credit_controller')[1]
        if group_id:
            browse_group = self.env['res.groups'].browse(group_id)
            for user in browse_group.users:
                credit_controller = user.partner_id.email
                user_email = self.user_id.partner_id.email
                mtp =self.env['mail.template']
                ir_model_data = self.env['ir.model.data']
                template_id = ir_model_data.get_object_reference('MKS_Tradex_Backend_2', 'sale_credit_controller_template')
                mail_tem=mtp.browse(template_id[1])
                mail_tem.send_mail(self.id,True)
                mail_tem.write({'email_from': user_email,'email_to': credit_controller})
        self.state = 'sale'
        return True
        
        
    def action_credit_approval(self):
        self.state = 'credit_approval'
        return True

        
        
            

