# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'MKS Tradex Backend',
    'version': '13.0.1.4',
    'category': 'Operations/Purchase',
    'description': """
This module allows you to manage your Purchase Agreements flow.
    Store keeper create Purchase Agreement
    Purchase Codinator Approve / Reject Purchase Agreement
    Purchase Manager Approve / Reject Purchase Agreement
    Purchase Codinator Create RFQ after Approve purchase agreement by purchase manager

""",
    'depends': ['purchase','purchase_requisition','sale_management','sale_renting'],
    'data': [
        'edi/mail_template.xml',
        'edi/purchase_mail_template.xml',
        'edi/sale_mail_template.xml',
        'edi/invoice_mail_template.xml',
        'security/purchase_security.xml',
        'security/account_security.xml',
        'views/purchase_requisition_views.xml',
        'views/purchase_order_views.xml',
        'views/sale_order_view.xml',
        'views/stock_picking.xml',
        'views/account_move_views.xml',
    ],
    
}
