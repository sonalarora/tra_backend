from odoo import _, fields, models, api


class Inventorygroups(models.Model):
    _name = "stock.groups"
    _parent_name = "parent_id"
    _rec_name = 'complete_name'

    name = fields.Char(required=True)
    parent_id = fields.Many2one('stock.groups', "Parent Group")
    complete_name = fields.Char(
        'Complete Name', compute='_compute_complete__group_name',
        store=True)

    @api.depends('name', 'parent_id.complete_name')
    def _compute_complete__group_name(self):
        for category in self:
            if category.parent_id:
                category.complete_name = '%s / %s' % (category.parent_id.complete_name, category.name)
            else:
                category.complete_name = category.name