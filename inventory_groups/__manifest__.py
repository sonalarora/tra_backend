# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Inventory Groups',
    'version': '1.0',
    'category': 'Inventory/Product',
    'summary': 'Enable to make arrangement in Groups',
    'description': "",
    'depends': ['base'],
    'data': [
       
        
        'security/ir.model.access.csv',
        'views/groups.xml',
    ],
    'demo': [
       
    ],
    'css': [],
    'installable': True,
    'application': True,
    'auto_install': False
}
