# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import api, fields, models, _


class HrEmployeePrivate(models.Model):
    
    _inherit = "hr.employee"


    home_address = fields.Char(string="Home Address")
    home_country_address = fields.Char(string="Home Country Address")
    name_of_airport = fields.Char(string="Name of Airport")
    relationship = fields.Char(string="Relationship")
    cat_of_staff = fields.Char(string="Category (If Staff or Labour)")
    work_loc = fields.Char(string="Work Location")
