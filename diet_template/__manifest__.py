# See LICENSE file for full copyright and licensing details.

{
    'name': 'Diet Template',
    'summary': 'Diet Plan Designing and Scheduling',
    'author': 'Serpent Consulting Services Pvt. Ltd.',
    'maintainer': 'Serpent Consulting Services Pvt. Ltd.',
    'license': 'LGPL-3',
    'category': 'Gym Management',
    'website': 'http://www.serpentcs.com',
    'version': '13.0.1.0.0',
    'sequence': 1,
    'depends': ['gym'],
    'data': [
        'security/ir.model.access.csv',
        'views/diet_view.xml',
        'views/project_view.xml',
        'views/project_task_view.xml',
        'views/menuitem_hide.xml',
        'views/sale_order_view.xml',
        'wizard/diet_schedule_view.xml',
        "views/partner_view.xml",
    ],
    'demo': [
        'demo/diet_plan_demo.xml',
    ],
    'images': ['static/description/gym2.jpg'],
    'installable': True,
    'price': 99,
    'currency': 'EUR',
}
