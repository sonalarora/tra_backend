# See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class ResPartner(models.Model):
    _inherit = "res.partner"

    member_diet_plan = fields.Integer(
        string='Diet Plan',
        compute='_compute_member_diet_plan'
    )

    def _compute_member_diet_plan(self):
        for rec in self:
            rec.member_diet_plan = \
                self.env['project.project'].search_count(
                    [('partner_id', '=', rec.id)])