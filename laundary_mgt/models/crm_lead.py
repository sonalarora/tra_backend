# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
import threading
from psycopg2 import sql
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, tools, SUPERUSER_ID
from odoo.tools.translate import _
from odoo.tools import email_re, email_split
from odoo.exceptions import UserError, AccessError
from odoo.addons.phone_validation.tools import phone_validation
from collections import OrderedDict, defaultdict




class Lead(models.Model):
    _inherit = "crm.lead"

    create_task = fields.Boolean(string='create task', default=False, copy=False)

    def action_create_task(self):
        for rec in self:
            rec.create_task = True
            task_ids = self.env['project.task'].create({
                        'project_id': 2,
                        'partner_id': rec.partner_id.id,
                        'user_id': '',
                        'name': rec.name,
                        'crm_id':self.id,
                        'is_fsm':True,

            })


