# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import datetime, timedelta
from functools import partial
from itertools import groupby

from odoo import api, fields, models, SUPERUSER_ID, _
from odoo.exceptions import AccessError, UserError, ValidationError
from odoo.tools.misc import formatLang, get_lang
from odoo.osv import expression
from odoo.tools import float_is_zero, float_compare



from werkzeug.urls import url_encode

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    check_trackcode = fields.Boolean(string='Check Track code', readonly=True, default=False, copy=False)

    def action_generate_tracking_code(self):
        for order in self:
            order.check_trackcode = True
            company_id = default=lambda self: self.env.company
            seq_date = fields.Datetime.now()
            for line in order.order_line:
                tracking_code = self.env['ir.sequence'].next_by_code('sale.order.line', sequence_date=seq_date) or _('New')
                line.tracking_code = self.name + tracking_code


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    _description = 'Sales Order Line'
    
    wash_type = fields.Many2one('wash.type', string='Wash Type')
    color = fields.Many2one('color.type', string='Color')
    cloth_name = fields.Many2one('cloth.type',string='Cloth Name',copy=False)
    tracking_code = fields.Char(string='Tracking Code',)
    remark = fields.Text('Remark',copy=False)
    upload_image = fields.Image("Upload Image")




class WashType(models.Model):
    _name = 'wash.type'
    _description = 'Wash Type'

    name = fields.Char(string='Name')

class ColorType(models.Model):
    _name = 'color.type'
    _description = 'Color Type'

    name = fields.Char(string='Name')

class ClothType(models.Model):
    _name = 'cloth.type'
    _description = 'Cloth Type'

    name = fields.Char(string='Name')

class ProjectTask(models.Model):
    _inherit = "project.task"

    cloth_name = fields.Many2one('cloth.type', string='Cloth Name',related='sale_line_id.cloth_name')
    wash_type = fields.Many2one('wash.type', string='Wash Type',related='sale_line_id.wash_type')
    color = fields.Many2one('color.type', string='Color', related='sale_line_id.color')
    tracking_code = fields.Char(string='Tracking Code',related='sale_line_id.tracking_code')
    remark = fields.Text('Remark',copy=False, related='sale_line_id.remark')
    crm_id = fields.Many2one('crm.lead', string='Crm Id')

class Project(models.Model):
    _inherit = "project.project"

    @api.model
    def create(self, vals):
        lst=[]
        project = super(Project, self).create(vals)
        stage_ids = self.env['project.task.type'].search([('all_project_check','=',True)])
        project.update({'type_ids': [(6, 0, stage_ids.ids)]})
        return project

class ProjectTaskType(models.Model):
    _inherit = 'project.task.type'

    all_project_check = fields.Boolean(string='Show All Project', default=False, copy=False)
