# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Laundary Management',
    'version': '1.1',
    'category': 'Sales/Sales',
    'summary': 'Laundary Management',
    'description': """
This module contains all the common features of Laundary Management.
    """,
    'depends': ['sale','sale_timesheet','crm'],
    'data': [
        'security/ir.model.access.csv',
        'data/ir_sequence_data.xml',
        'views/sale_views.xml',
        'views/sale_views.xml',
        'views/project_task_view.xml',
        'views/crm_lead_view.xml',
        'report/barcode_report.xml',
        'report/menu.xml'
    ],
    'demo': [
    ],
    'qweb': [
       
    ],
    'installable': True,
    'auto_install': False
}
