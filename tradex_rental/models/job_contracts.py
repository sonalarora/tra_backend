from odoo import fields, models


class JobMaster(models.Model):
    _name = "job.master"

    job_no = fields.Float("Job Number", required=True)
    name = fields.Char("Name")


class ContractMaster(models.Model):
    _name = "contract.master"

    name = fields.Char("Name")


class SaleOrderRental(models.Model):
    _inherit = "sale.order"

    job_master = fields.Many2one('job.master', string='Job Master')
    contract_master = fields.Many2one('contract.master', string='Contract Master')
