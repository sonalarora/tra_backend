# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Tradex Rental',
    'version': '1.0',
    'category': 'Rental',
    'summary': 'Add masters for Jobs and Contracts',
    'description': "",
    'depends': ['sale_renting'],
    'data': [
        'views/job_contract.xml',
        'security/ir.model.access.csv'
    ],
    'installable': True,
    'application': True,
    'auto_install': False
}
