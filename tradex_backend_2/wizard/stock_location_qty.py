# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError

class stock_location_quantity(models.TransientModel):
    _name = "stock.location.quantity"
    _description = 'location quantity'
    
    product_id = fields.Many2one('product.product', string='Product')
    qty = fields.Float('Quantity On Hand')
    location_lines = fields.One2many('stock.location.quantity.line','slq_id', string='Location Quantity Lines')
    
    @api.model
    def default_get(self, fields):
        res = super(stock_location_quantity,self).default_get(fields)
        if res.get('product_id'):
            product_id = self.env['product.product'].browse(res.get('product_id'))
            res.update({
                'qty':product_id.qty_available
            })
            line_ids = self.env['stock.move.location.lines'].search([('product_id','=',product_id.id)])
            vals = []
            for line in line_ids:
                vals.append((0,0,{
                    'location_id':line.location_id and line.location_id.id or False,
                    'quantity':line.qty or 0.0,
                    'company_id':line.company_id and line.company_id.id or False,
                }))
            res.update({'location_lines':vals})
        return res


class stock_location_quantity_line(models.TransientModel):
    _name = "stock.location.quantity.line"
    _description = 'location quantity'
    
    location_id = fields.Many2one('stock.location', string='Location')
    quantity = fields.Float('Quantity')
    slq_id = fields.Many2one('stock.location.quantity', string='Stock Location')
    company_id = fields.Many2one('res.company', string='Owner')

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:    
    
