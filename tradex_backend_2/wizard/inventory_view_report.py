    # -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
import itertools
from operator import itemgetter
import operator
# ========For Excel=======
from io import BytesIO
import xlwt
from xlwt import easyxf, Formula
import base64
from datetime import  datetime
# =======================

class inventory_view_report(models.TransientModel):
    _name = "inventory.view.report"
    _description = 'Inventory View Report'
    
    product_ids = fields.Many2many('product.product', string='Products')
    excel_file = fields.Binary('Excel File')
    
    def create_excel_header(self,worksheet):
        header_style = easyxf('font:height 300;pattern: pattern solid, fore_color silver_ega; align: horiz center, vert center;font:bold True;')
        sub_header = easyxf('font:height 210;pattern: pattern solid, fore_color silver_ega;font:bold True;')
        content = easyxf('font:height 200;')
        worksheet.write_merge(2, 3, 2, 4, 'Inventory View Report ', header_style)
        row=5
        
        return worksheet, row+1 
    
    def create_excel_table_header(self,row,worksheet,company_ids):
         header_style = easyxf('font:height 150;pattern: pattern solid, fore_color silver_ega; align: horiz center, vert center;font:bold True;' "borders: top thin,bottom thin,right thin, left thin")
         worksheet.write(row, 0,'ITEM CODE', header_style)
         worksheet.write(row, 1,'DESCRIPTION', header_style)
         worksheet.write(row, 2,'TOTAL QUANTITY', header_style)
         worksheet.write(row, 3,'LOCATION', header_style)
         
         col=4
         for company in company_ids:
            worksheet.write(row, col,company.name, header_style)
            col+=1
            
         row+=1
         
         return worksheet, row
         
        
        
    def get_company(self):
        sql_query = """select DISTINCT sl.company_id from stock_move_location_lines as smll LEFT JOIN stock_location as sl ON sl.id = smll.location_id LEFT JOIN res_company as rc ON rc.id=sl.company_id where smll.product_id in %s"""
        params = (tuple(self.product_ids.ids),)
        self.env.cr.execute(sql_query, params)
        results = self.env.cr.dictfetchall()
        company_ids = [result.get('company_id') for result in results]
        company_ids = self.env['res.company'].browse(company_ids)
        return company_ids
        
    def get_location(self,product_id):
        sql_query = """select DISTINCT location_id from stock_move_location_lines where product_id = %s"""
        params = (product_id.id,)
        self.env.cr.execute(sql_query, params)
        results = self.env.cr.dictfetchall()
        location_ids = [result.get('location_id') for result in results]
        location_ids = self.env['stock.location'].browse(location_ids)
        return location_ids
    
    
    def get_quantity(self,product,location,company):
        sql_query = """select sum(smll.qty) from stock_move_location_lines as smll LEFT JOIN stock_location as sl ON sl.id = smll.location_id LEFT JOIN res_company as rc ON rc.id=sl.company_id where smll.product_id = %s and smll.location_id = %s and sl.company_id = %s"""
        params = (product.id,location.id,company.id)
        self.env.cr.execute(sql_query, params)
        results = self.env.cr.dictfetchall()[0]
        if results.get('sum'):
            return results.get('sum')
        return 0.0
        
    
    def create_excel_table_values(self,row,worksheet,company_ids):
         header_style = easyxf('font:height 150;pattern: pattern solid, fore_color silver_ega; align: horiz center, vert center;font:bold True;' "borders: top thin,bottom thin,right thin, left thin")
         content = easyxf('font:height 200; align: vert center;' "borders: top thin,bottom thin,right thin, left thin",num_format_str='0.00')
         content_bold = easyxf('font:height 200; align: vert center;font:bold True;' "borders: top thin,bottom thin,right thin, left thin",num_format_str='0.00')
         for product in self.product_ids:
            location_ids = self.get_location(product)
            if location_ids:
                worksheet.write(row, 0,product.default_code, content)
                worksheet.write(row, 1,product.name, content)
                worksheet.write(row, 2,product.qty_available, content)
                com_sum_row = row
                row+=1
                com_sum=[]
                for company in company_ids:
                    com_sum.append({
                        'company_id':company.id,
                        'qty':0.0
                    })
                for location in location_ids:
                     worksheet.write(row, 3, location.display_name, content)
                     col=4
                     total_qty = 0.0
                     for company_id in company_ids:
                        qty = self.get_quantity(product,location,company_id)
                        total_qty += qty
                        if qty:
                            for com in com_sum:
                                if com.get('company_id') == company_id.id:
                                    com.update({
                                        'qty':com.get('qty') + qty
                                    }) 
                        
                        worksheet.write(row, col, qty, content)
                        col+=1
                     worksheet.write(row, 2, total_qty, content_bold)
                     row+=1
                s_c = 3
                for company in company_ids:
                    s_c += 1
                    for com in com_sum:
                        if company.id == com.get('company_id'):
                             worksheet.write(com_sum_row, s_c, com.get('qty'), content_bold)
                row+=1
         
         return worksheet, row
         
    
    def print_report(self):
        workbook = xlwt.Workbook()
        filename = 'Inventory View.xls'
        worksheet = workbook.add_sheet('Inventory Views')
        worksheet,row = self.create_excel_header(worksheet)
        for i in range(0,200):
            worksheet.col(i).width = 150 * 30
            if i == 1:
                worksheet.col(i).width = 230 * 30
            
        for i in range(5,50):
            worksheet.row(i).height_mismatch = True
            worksheet.col(i).width = 130 * 30
            worksheet.row(i).height = 20 * 20
            
        company_ids = self.get_company()
        worksheet,row = self.create_excel_table_header(row,worksheet,company_ids)
        worksheet,row = self.create_excel_table_values(row,worksheet,company_ids)
        
#        company_ids = self.env['res.company'].browse(company)
#        print ("======",lines,company_ids)
#        def create_excel_table_val(self,worksheet,company):
        fp = BytesIO()
        workbook.save(fp)
        fp.seek(0)
        excel_file = base64.encodestring(fp.read())
        fp.close()
        self.write({'excel_file': excel_file})
        
        active_id = self.ids[0]
        return {'type': 'ir.actions.act_url', 'url': 'web/content/?model=inventory.view.report&download=true&field=excel_file&id=%s&filename=%s' % (active_id, filename), 'target': 'new', }
        
    
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:    
    
