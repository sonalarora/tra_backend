# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, SUPERUSER_ID, _
from odoo.exceptions import ValidationError

class stock_move(models.Model):
    _inherit = 'stock.move'
    
    move_location_lines = fields.One2many('stock.move.location.lines','move_id', string='Move Location Lines')
    
    
    # @api.constrains('move_location_lines','move_location_lines.qty')
    # def check_location_quantity(self):
    #     for move in self:
    #         if move.move_location_lines:
    #             qty = 0
    #             for line in move.move_location_lines:
    #                 qty += line.qty
    #
    #             if qty > move.product_uom_qty:
    #                 raise ValidationError(_("Location Quantity must less or equal %s Quantity")% move.product_uom_qty)

    @api.constrains('move_location_lines','move_location_lines.qty')
    def check_location_quantity(self):
        for move in self:
            if move.move_line_nosuggest_ids:
                base_qty = 0
                location_qty = 0
                for base_move in move.move_line_nosuggest_ids:
                    base_qty += base_move.qty_done
                for location_move in move.move_location_lines:
                    location_qty += location_move.qty
                if location_qty > base_qty:
                    raise ValidationError(_("Location Quantity must less or equal %s Quantity") % base_qty)
            else:
                raise ValidationError(_("Please add some done quantity in above table"))

class stock_move_location_lines(models.Model):
    _name = "stock.move.location.lines"

    def get_company_id(self):
        company_id = False
        if self.move_id and self.move_id.picking_id:
            if self.move_id.picking_id and self.move_id.picking_id.location_dest_id:
                if self.move_id.picking_id.location_dest_id.company_id:
                    company_id = self.move_id.picking_id.location_dest_id.company_id.id
        return company_id

    product_id = fields.Many2one('product.product', string='Product', required="1")
    qty = fields.Float(string='Quantity', required="1")
    uom = fields.Many2one('uom.uom', string='UOM', required="1")
    location_id = fields.Many2one('stock.location', string='Location', required="1")
    company_id = fields.Many2one('res.company', string='Owner', default=get_company_id)
    move_id = fields.Many2one('stock.move', string='Move')

        
            

