# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Tradex Backend',
    'version': '13.0.1.0',
    'category': 'Stock',
    'description': """
    This module allows you to add location in stock move
d
""",
    'depends': ['sale_management','sale_stock'],
    'data': [
        'security/ir.model.access.csv',
        'wizard/stock_location_qty_views.xml',
        'wizard/inventory_view_report.xml',
        'views/stock_move_views.xml',
        'views/sale_order_views.xml',
        'views/inventory_view.xml',
    ],
    
}
