# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Tra_Backend',
    'version': '1.0',
    'category': 'Product',
    'sequence': 5,
    'summary': 'Product',
    'author': 'Masterkey',
    'description': "",
    'website': 'https://www.odoo.com/page/crm',
    'depends': ['product','base','sale_renting','account'
    ],
    'data': [
        'views/product_views.xml',
	    'views/partner_view.xml',
        'views/account_move_view.xml'
    ],
    'demo': [
       
    ],
    'css': [],
    'installable': True,
    'application': True,
    'auto_install': False
}
