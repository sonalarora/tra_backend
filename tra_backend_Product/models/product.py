# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
from psycopg2 import sql, extras
from datetime import datetime, timedelta, date

from odoo import api, fields, models, tools, SUPERUSER_ID


class ProductProduct(models.Model):
    _inherit = "product.template"

    width = fields.Float('Width', digits='Stock Weight')
    length = fields.Float('Length', digits='Stock Weight')
    height = fields.Float('Height', digits='Stock Weight')
    model_grade = fields.Char('Model/Grade')
    rental_group_id = fields.Char('Rental Group')
    group_id = fields.Many2one(
        'stock.groups', 'Group', help="Select group for the current product")
