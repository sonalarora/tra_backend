# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import RedirectWarning, UserError, ValidationError, AccessError
from datetime import date


class AccountMove(models.Model):
    _inherit = "account.move"


    # ==== Business fields ====
    overdue_by = fields.Float(string='Overdue By', index=True)#, compute='calculate_overdue_date')


    def calculate_overdue_date(self):
        current_date = date.today()
        overdue_date = self.invoice_due_date - current_date
        self.overdue_by = overdue_date.days

