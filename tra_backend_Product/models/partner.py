import time
import logging

from odoo import api, fields, models, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import ValidationError
from odoo.addons.base.models.res_partner import WARNING_MESSAGE, WARNING_HELP
from psycopg2 import sql, DatabaseError

class ResPartner(models.Model):
    _inherit = 'res.partner'

    street3 = fields.Char('Steet3')
    street4 = fields.Char('Steet4')
    street5 = fields.Char('Steet5')
