# -*- coding: utf-8 -*-

from odoo import fields, models,api


class AccountAnalyticAccount(models.Model):
    _inherit = 'account.analytic.account'


    is_location = fields.Boolean(
        string='Is Location',
        compute='compute_is_location',
        store=True
    )


    @api.depends('group_id')
    def compute_is_location(self):
        for record in self:
            if record.group_id.name=='Location':
                record.is_location=True
            else:
                record.is_location=False
