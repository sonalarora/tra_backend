# See LICENSE file for full copyright and licensing details.

from . import member
from . import trainer
from . import product_template
from . import membership
from . import diet
from . import workout
from . import lead
from . import company
from . import gym_skills
from . import project_task
from . import sale
from . import calendar_event
from . import project_document
from . import project_document_case
from . import project_document_category
from . import project_document_group
from . import project_document_name
from . import project_document_opposite
from . import project_other
from . import account_analytic_account
from . import res_partner_credit_card
from . import res_partner_credit_card_type