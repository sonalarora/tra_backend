
from odoo import api, fields, models, _

class ProjectDocumentCategory(models.Model):

    _name = 'project.document.category'
    _description = "Documents Categories"
    _rec_name = "name"

    # --------------------------------------------------------------------
    # FIELDS
    # --------------------------------------------------------------------

    name = fields.Char(
        string='Name',
    )