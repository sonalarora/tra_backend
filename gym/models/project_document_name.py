# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class ProjectDocumentName(models.Model):

    _name = 'project.document.name'
    _description = "Documents Name"
    _rec_name = "name"

    # --------------------------------------------------------------------
    # FIELDS
    # --------------------------------------------------------------------

    name = fields.Char(
        string='Name',
    )