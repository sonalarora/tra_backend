# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from datetime import date
from odoo.exceptions import ValidationError

class ResPartnerCreditCard(models.Model):

    _name = 'res.partner.credit.card'
    _rec_name = 'name'

    # --------------------------------------------------------------------
    # FIELDS
    # --------------------------------------------------------------------

    name = fields.Char(
        string='Name',
    )

    type_id = fields.Many2one(
        comodel_name='res.partner.credit.card.type',
        string='Type',
    )

    number = fields.Integer(
        string='Number',
    )

    expiry_date = fields.Date(
        string='Expiry Date'
    )

    ccb_number = fields.Char(
        string='CCB Number',
    )

    partner_id = fields.Many2one(
        comodel_name='res.partner',
        string='Partner',
    )