# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class ProjectDocumentGroup(models.Model):

    _name = 'project.document.group'
    _description = "Documents Group"
    _rec_name = "category_id"

    # --------------------------------------------------------------------
    # FIELDS
    # --------------------------------------------------------------------

    category_id = fields.Many2one(
    	comodel_name="project.document.category",
        string='Category',
    )

    document_name_ids = fields.Many2many(
        comodel_name="project.document.name",
        string='Documents',
    )