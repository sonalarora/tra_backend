# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from datetime import date
from odoo.exceptions import ValidationError

class ResPartnerCreditCardType(models.Model):

    _name = 'res.partner.credit.card.type'
    _rec_name = 'name'

    # --------------------------------------------------------------------
    # FIELDS
    # --------------------------------------------------------------------

    name = fields.Char(
        string='Name',
    )