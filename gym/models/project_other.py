# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from datetime import date,datetime

class ProjectDocumentOther(models.Model):

    _name = 'project.document.other'
    _description = "Other Documents"
    _rec_name = "document_name_id"

    # --------------------------------------------------------------------
    # FIELDS
    # --------------------------------------------------------------------

    document_name_id = fields.Many2one(
    	comodel_name="project.document.name",
        string='Document Name',
    )

    partner_id = fields.Many2one(
        comodel_name='res.partner',
        string='Partner',
    )

    datas = fields.Binary(
        string='Document',
    )

    datas_fname = fields.Char('Filename')

    is_document_set = fields.Boolean(
        string='Is Document Set',
        compute='_compute_document_set',
        store=True,
    )

    user_id = fields.Many2one(
        comodel_name='res.users',
        string='Uploaded By',
        readonly=True,
    )

    date = fields.Datetime(
        string='Date',
        readonly=True,
    )

    # --------------------------------------------------------------------
    # METHODS
    # --------------------------------------------------------------------

    @api.depends('datas')
    def _compute_document_set(self):
        for doc in self:
            if doc.datas:
               doc.is_document_set = True
            else:
                doc.is_document_set = False

    @api.model
    def create(self,vals):
        if vals.get('datas'):
            vals['user_id'] = self.env.user.id
            vals['date'] = datetime.now()
        res = super(ProjectDocumentOther,self).create(vals)
        return res

    def write(self, values):
        if values.get('datas'):
            values['user_id'] = self.env.user.id
            values['date'] = datetime.now()
        return super(ProjectDocumentOther,self).write(values)