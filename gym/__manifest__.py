# See LICENSE file for full copyright and licensing details.

{
    'name': 'GYM Management',
    'summary': 'This module is used for Gym Management with Membership,'
    ' Trainer, and Equipment.',
    'version': '13.0.1.0.0',
    'license': 'LGPL-3',
    'author': 'Serpent Consulting Services Pvt. Ltd.',
    'maintainer': 'Serpent Consulting Services Pvt. Ltd.',
    'website': 'http://www.serpentcs.com',
    'category': 'Gym Management',
    'depends': [
        'sale_management',
        'hr',
        'crm',
        'project',
        'membership',
    ],
    
    'data': [
        'data/inactive_rule_data.xml',
        'security/security.xml',
        'security/ir.model.access.csv',
        'data/member_sequence.xml',
        'data/membership_plan_sequence.xml',
        'data/equipment_demo.xml',
        'data/email_template.xml',
        'data/membership_scheduler.xml',
        'data/service_demo.xml',
        'data/exercise_for_demo.xml',
        'data/exercise_demo.xml',
        'data/mail_templates.xml',
        'views/menuitem_hide.xml',
        'views/product_template.xml',
        'views/membership_view.xml',
        'views/project_task_view.xml',
        'views/member_view.xml',
        'views/trainer_view.xml',
        'views/workout_view.xml',
        'views/company_view.xml',
        'views/gym_skills_view.xml',
    ],
    
    'demo': [
        'demo/exercise_exercise_demo.xml',
        'demo/user_demo.xml',
        'demo/pedo_meter_demo.xml',
        'demo/food_demo.xml',
        'demo/membership_demo.xml'
    ],
    'images': ['static/description/gym.jpg'],
    'application': True,
    'installable': True,
    'price': 199,
    'currency': 'EUR',
}
