{
    'name': 'Reports',
    'version': '13.0.1.0.0',
    'summary': 'Reports for sale, account etc.',
    'description': """
        """,
    'category': 'tools',
    'author': "",
    'company': '',
    'maintainer': '',
    'website': "",
    'depends': [
        'base', 'account', 'bi_professional_reports_templates'
    ],
    'data': [
        'views/invoice_report.xml',
        'views/sale_report.xml',
        'views/account_report.xml',
        'views/report_payment_receipt.xml',
    ],
    'demo': [],
    'images': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}
