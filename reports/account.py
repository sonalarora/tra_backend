from odoo import models, api, fields
from datetime import date


class AccountMove(models.Model):
    _inherit = "account.move"
    txn_id = fields.Char("Transaction ID")
    signed_by = fields.Char("Printed by", compute='compute_signature')
    print_date = fields.Date("Print date", compute='compute_print_date')
    note = fields.Text("Note")

    def compute_print_date(self):
        for rec in self:
            rec.print_date = date.today()


    def compute_signature(self):
        for rec in self:
            rec.signed_by = self.env.user.name
